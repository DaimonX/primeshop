from django import forms
from django.forms.models import modelformset_factory

from .models import Heater
from crispy_forms.layout import Layout
from crispy_forms.helper import FormHelper
from crispy_forms.bootstrap import PrependedText, Field


class HeaterStuffForm(forms.ModelForm):
    class Meta:
        model = Heater
        fields = [
            "price",
            "sale_price",
            "inventory",
            "active",
        ]

    def __init__(self, *args, **kwargs):
        super(HeaterStuffForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.form_class = 'form-inline'
        # self.helper.field_template = 'bootstrap3/layout/inline_field.html'
        self.helper.field_class = 'small-input'
        self.helper.form_show_labels = False
        self.helper.layout = Layout(
            Field("price", css_class="price"),
            Field("sale_price", css_class="sale_pricep"),
            Field("inventory", css_class="inventory"),
            Field("active", css_class="checkbox"),
        )
        super(HeaterStuffForm, self).__init__(*args, **kwargs)
HeaterStuffFormSet = modelformset_factory(Heater, form=HeaterStuffForm, extra=0)

