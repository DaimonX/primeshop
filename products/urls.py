from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib import admin
from catalog import views

from .views import HeaterDetailView, HeaterListView, HeaterStuffListView

urlpatterns = [

    url(r'stuff/', HeaterStuffListView.as_view(), name='heater_stuff'),
    url(r'^(?P<pk>\d+)/$', HeaterDetailView.as_view(), name='heater_detail'),
    url(r'^', HeaterListView.as_view(), name='heater_list'),
    # url(r'^(?P<id>\d+)', 'products.views.product_detail_view_func', name='product_detail_view_func'),
]

# urlpatterns = patterns('',
#     url(r'^(?P<id>\d+)', 'products.views.product_detail_view_func', name='product_detail_view_func'),
# )
