# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0008_auto_20151126_1837'),
    ]

    operations = [
        migrations.AlterField(
            model_name='heater',
            name='inventory',
            field=models.IntegerField(blank=True, null=True, max_length=3, verbose_name='Склад'),
        ),
    ]
