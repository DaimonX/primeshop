# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='heater',
            name='description',
            field=models.TextField(verbose_name='Описание', blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='heater',
            name='inventory',
            field=models.IntegerField(verbose_name='Склад', blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='heater',
            name='test_pressure',
            field=models.CharField(default='10', verbose_name='Максю дав. на прочность, МПа', max_length=2),
        ),
        migrations.AlterField(
            model_name='heater',
            name='work_pressure',
            field=models.DecimalField(max_digits=3, default='1', verbose_name='Макс.раб. дав., МПа', decimal_places=1),
        ),
    ]
