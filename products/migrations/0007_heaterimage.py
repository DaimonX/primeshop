# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0006_auto_20151125_1831'),
    ]

    operations = [
        migrations.CreateModel(
            name='HeaterImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('image', models.ImageField(upload_to='products/')),
                ('heater', models.ForeignKey(to='products.Heater')),
            ],
        ),
    ]
