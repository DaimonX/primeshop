# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Heater',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('title', models.CharField(max_length=120, verbose_name='Название', default='Лидея')),
                ('heater_type', models.CharField(help_text='ЛК (4 боковых патрубка), ЛУ (4 боковых и 2 донных патрубка)', max_length=2, choices=[('lu', 'ЛУ'), ('lk', 'ЛК')], verbose_name='Тип', default='lk')),
                ('panels_convectors', models.CharField(help_text='1 цифра: кол-во панелей, 2 цифра: кол-во рядов оребрения', max_length=2, choices=[('10', '10'), ('11', '11'), ('20', '20'), ('21', '21'), ('22', '22'), ('30', '30'), ('33', '33')], verbose_name='Вид', default='22')),
                ('height', models.IntegerField(choices=[(300, '300 мм'), (500, '500 мм'), (600, '600 мм'), (700, '700 мм')], verbose_name='Высота', default=500)),
                ('description', models.TextField(blank=True, verbose_name='Описание', null=True)),
                ('price', models.DecimalField(decimal_places=2, max_digits=20, verbose_name='Цена')),
                ('sale_price', models.DecimalField(decimal_places=2, max_digits=20, blank=True, verbose_name='Распродажная Цена', null=True)),
                ('active', models.BooleanField(default=True)),
                ('created_at', models.DateTimeField(verbose_name='создан', auto_now_add=True)),
                ('updated_at', models.DateTimeField(verbose_name='обновлен', auto_now=True)),
                ('inventory', models.IntegerField(blank=True, verbose_name='Склад', null=True)),
                ('work_pressure', models.DecimalField(decimal_places=1, max_digits=3, verbose_name='Макс.раб. дав., МПа', default='1')),
                ('test_pressure', models.CharField(max_length=2, verbose_name='Максю дав. на прочность, МПа', default='10')),
                ('max_temp_coolant', models.IntegerField(verbose_name='Макс, t℃', default=120)),
                ('heat_power', models.IntegerField(blank=True, verbose_name='Мощность при ΔT=70℃, Вт', null=True)),
                ('weight', models.DecimalField(decimal_places=1, max_digits=4, blank=True, verbose_name='Масса, кг', null=True)),
                ('сapacity_liter', models.DecimalField(decimal_places=1, max_digits=4, blank=True, verbose_name='Объем, л', null=True)),
            ],
            options={
                'verbose_name_plural': 'Радиаторы',
                'verbose_name': 'Радиатор',
            },
        ),
        migrations.CreateModel(
            name='Length',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('length', models.IntegerField(unique=True, choices=[(400, '400 мм'), (500, '500 мм'), (600, '600 мм'), (700, '700 мм'), (800, '800 мм'), (900, '900 мм'), (1000, '1000 мм'), (1100, '1100 мм'), (1200, '1200 мм'), (1300, '1300 мм'), (1400, '1400 мм'), (1500, '1500 мм'), (1600, '1600 мм'), (1700, '1700 мм'), (1800, '1800 мм'), (1900, '1900 мм'), (2000, '2000 мм'), (2100, '2100 мм'), (2200, '2200 мм'), (2300, '2300 мм'), (2400, '2400 мм'), (2500, '2500 мм'), (2600, '2600 мм'), (2700, '2700 мм'), (2800, '2800 мм'), (2900, '2900 мм'), (3000, '3000 мм')], verbose_name='Длина радиатора')),
            ],
            options={
                'verbose_name_plural': 'Длины радиаторов',
                'verbose_name': 'Длина радиатора',
            },
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('title', models.CharField(max_length=120, verbose_name='Продукт')),
                ('description', models.TextField(blank=True, null=True)),
                ('active', models.BooleanField(default=True)),
                ('created_at', models.DateTimeField(verbose_name='создан', auto_now_add=True)),
                ('updated_at', models.DateTimeField(verbose_name='обновлен', auto_now=True)),
            ],
            options={
                'verbose_name_plural': 'Продукты',
                'verbose_name': 'Продукт',
            },
        ),
        migrations.AddField(
            model_name='heater',
            name='length',
            field=models.ForeignKey(to='products.Length', verbose_name='Длина'),
        ),
        migrations.AddField(
            model_name='heater',
            name='product',
            field=models.ForeignKey(to='products.Product', verbose_name='Продукт', default=1),
        ),
    ]
