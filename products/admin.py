from django.contrib import admin

# Register your models here.
from .models import Heater, Length, Product, HeaterImage


# class LengthAdmin(admin.TabularInline):
#      model = Length

#
# class ProductAdmin(admin.ModelAdmin):
#     inlines = (LengthAdmin,)

@admin.register(Heater)
class HeaterAdmin(admin.ModelAdmin):
    model = Heater
    list_display = ("product", "title",  "heater_type", "panels_convectors", "height", "length", "price", "sale_price")
    #fields = ("product", "title", "heater_type", "panels_convectors", "height", "length", "price", "sale_price", "active")



admin.site.register(Product)
admin.site.register(Length)
admin.site.register(HeaterImage)
