# -*- coding: utf-8 -*-
from django.db import models
from django.core.urlresolvers import reverse
from django.utils.text import slugify


# Create your models here.
class HeaterQuerySet(models.query.QuerySet):
    def acive(self):
        return self.filter(active=True)


class HeaterManager(models.Manager):
    def get_queryset(self):
        return HeaterQuerySet(self.model, using=self._db)

    def all(self, *args, **kwargs):
        return self.get_queryset()


class Product(models.Model):
    title = models.CharField('Продукт', max_length=120)
    description = models.TextField(blank=True, null=True)
    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='создан')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='обновлен')

    # slug

    class Meta:
        verbose_name = 'Продукт'
        verbose_name_plural = 'Продукты'

    def __str__(self):
        return self.title

    # product Images

    # product Category


class Heater(models.Model):
    TYPE_CHOICES = (('lu', 'ЛУ'), ('lk', 'ЛК'))
    PANELS_CONVECTORS_CHOICES = (('10', '10'), ('11', '11'), ('20', '20'), ('21', '21'),
                                 ('22', '22'), ('30', '30'), ('33', '33'),)
    HEIGHT_CHOICES = ((300, '300 мм'), (500, '500 мм'), (600, '600 мм'), (700, '700 мм'),)
    title = models.CharField('Название', max_length=120, default='Лидея')
    heater_type = models.CharField('Тип', max_length=2, choices=TYPE_CHOICES,  default='lk',
                                   help_text='ЛК (4 боковых патрубка), ЛУ (4 боковых и 2 донных патрубка)')
    panels_convectors = models.CharField('Вид', max_length=2, choices=PANELS_CONVECTORS_CHOICES,
                                         default='22',
                                         help_text='1 цифра: кол-во панелей, 2 цифра: кол-во рядов оребрения')
    height = models.IntegerField('Высота', choices=HEIGHT_CHOICES, default=500)
    description = models.TextField('Описание', blank=True, null=True)
    price = models.DecimalField('Цена', decimal_places=2, max_digits=20)
    sale_price = models.DecimalField('Распродажная Цена', decimal_places=2, max_digits=20, null=True, blank=True)
    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='создан')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='обновлен')
    inventory = models.IntegerField('Склад', null=True, blank=True, max_length=3)  # refer none === unlimited
    work_pressure = models.DecimalField(verbose_name='Макс.раб. дав., МПа', default='1', max_digits=3, decimal_places=1)
    test_pressure = models.CharField(verbose_name='Максю дав. на прочность, МПа', default='10', max_length=2)
    max_temp_coolant = models.IntegerField(verbose_name='Макс, t\u2103', default=120)
    # connection_type
    heat_power = models.IntegerField(verbose_name='Мощность при \u0394T=70\u2103, Вт', null=True, blank=True)
    weight = models.DecimalField(verbose_name='Масса, кг', decimal_places=1, max_digits=4, null=True, blank=True)
    сapacity_liter = models.DecimalField(verbose_name='Объем, л', decimal_places=1, max_digits=4, null=True, blank=True)

    # relations
    length = models.ForeignKey('Length', verbose_name='Длина')
    product = models.ForeignKey(Product, default=1, verbose_name='Продукт')

    # manager
    objects = HeaterManager()

    # product Images
    # product Category

    class Meta:
        verbose_name = 'Радиатор'
        verbose_name_plural = 'Радиаторы'

    def __str__(self):
            return '%s, %s, %s-%s, %sx%s' % (
                self.product,
                self.title,
                self.get_heater_type_display(),
                self.panels_convectors,
                self.height,
                self.length
            )

    def get_absolute_url(self):
        return reverse("heater_detail", kwargs={"pk": self.pk})

    def get_price(self):
        if self.sale_price is not None:
            return self.sale_price
        else:
            return self.price


class Length(models.Model):
    LENGTH_CHOICES = (
        (400, '400 мм'), (500, '500 мм'), (600, '600 мм'), (700, '700 мм'), (800, '800 мм'),
        (900, '900 мм'), (1000, '1000 мм'), (1100, '1100 мм'), (1200, '1200 мм'), (1300, '1300 мм'),
        (1400, '1400 мм'), (1500, '1500 мм'), (1600, '1600 мм'), (1700, '1700 мм'), (1800, '1800 мм'),
        (1900, '1900 мм'), (2000, '2000 мм'), (2100, '2100 мм'), (2200, '2200 мм'), (2300, '2300 мм'),
        (2400, '2400 мм'), (2500, '2500 мм'), (2600, '2600 мм'), (2700, '2700 мм'), (2800, '2800 мм'),
        (2900, '2900 мм'), (3000, '3000 мм'),
    )
    length = models.IntegerField('Длина радиатора', choices=LENGTH_CHOICES, unique=True)

    class Meta:
        verbose_name = 'Длина радиатора'
        verbose_name_plural = 'Длины радиаторов'
        # unique_together = ('length',)

    def __str__(self):
        return self.get_length_display()


def image_upload_to(instance, filename):
    slug = slugify("%s%s%sx%s" % (instance.heater.heater_type, instance.heater.panels_convectors, instance.heater.height, instance.heater.length))
    return "products/%s/%s" % (slug, filename)


class HeaterImage(models.Model):
    heater = models.ForeignKey(Heater)
    image = models.ImageField(upload_to=image_upload_to)

    def __str__(self):
        return self.heater.title
