from django.db.models import Q
from django.http import Http404
from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.utils import timezone
from django.contrib import messages
from braces import views
from pprint import pprint
# Create your views here.


from .models import Product, Heater

from .forms import HeaterStuffFormSet


class HeaterListView(ListView):
    model = Heater
    queryset = Heater.objects.all()
    # template_name = "<appname>/<modelname>_detail.html"

    def get_context_data(self, *args, **kwargs):
        context = super(HeaterListView, self).get_context_data(*args, **kwargs)
        context["now"] = timezone.now()
        return context

    def get_queryset(self, *args, **kwargs):
        qs = super(HeaterListView, self).get_queryset(*args, **kwargs).filter(active=True)
        query = self.request.GET.get("q")
        if query:
            qs = self.model.objects.filter(
                Q(description__icontains=query) |
                Q(title__icontains=query)
            )
        return qs


class HeaterStuffListView(views.LoginRequiredMixin, views.StaffuserRequiredMixin, ListView):
    model = Heater
    queryset = Heater.objects.all()
    template_name = 'products/heater_stuff_list.html'

    def get_context_data(self, *args, **kwargs):
        context = super(HeaterStuffListView, self).get_context_data(*args, **kwargs)
        context["formset"] = HeaterStuffFormSet(queryset=self.get_queryset())
        return context

    def get_queryset(self, *args, **kwargs):
        qs = super(HeaterStuffListView, self).get_queryset(*args, **kwargs)
        query = self.request.GET.get("q")
        if query:
            qs = self.model.objects.filter(
                Q(description__icontains=query) |
                Q(title__icontains=query)
            )
        return qs

    def post(self, request, *args, **kwargs):
        formset = HeaterStuffFormSet(request.POST, request.FILES)
        if formset.is_valid():
            formset.save(commit=False)
            for form in formset:
                form.save()
            messages.success(request, "Список обновлен")
            return redirect("heater_stuff")
        raise Http404


class HeaterDetailView(DetailView):
    model = Heater
    # template_name = "<appname>/<modelname>_detail.html"


def product_detail_view_func(request, id):
    # product_instance = Product.objects.get(id=id)
    product_instance = get_object_or_404(Product, id=id)

    template = "products/heater_detail.html"
    context = {
        "object": product_instance
    }
    return render(request, template, context)
