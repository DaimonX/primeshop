from django import forms
import re

from .models import Product

class ContactForm(forms.Form):
    full_name = forms.CharField(required=False)
    email = forms.EmailField()
    message = forms.CharField()


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ("name", "product_type")
        verbose_name = 'Поломка'
        verbose_name_plural = 'Поломки'

    def clean_product_type(self):
        product_type_raw = self.cleaned_data.get('product_type')
        #match = re.match(r'ТИП-[1-3][0-3]$', product_type_raw)
        match = re.match(r'Л[КУ]$', product_type_raw)
        if match is None:
            raise forms.ValidationError("Пожалуйста введите корректный тип")
        return product_type_raw

