# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='product_convectors',
            field=models.IntegerField(default=0, verbose_name='Количество панелей', max_length=1, choices=[(0, '0'), (1, '1'), (2, '2'), (3, '3')]),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='product',
            name='product_panels',
            field=models.IntegerField(default=0, verbose_name='Количество панелей', max_length=1, choices=[(1, '1'), (2, '2'), (3, '3')]),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='product',
            name='name',
            field=models.CharField(verbose_name='Имя', max_length=50),
        ),
        migrations.AlterField(
            model_name='product',
            name='product_type',
            field=models.CharField(choices=[('ЛУ', 'ЛУ'), ('ЛК', 'ЛК')], verbose_name='Тип', max_length=2),
        ),
    ]
