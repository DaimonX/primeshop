# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0002_auto_20151112_2318'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='product',
            options={'verbose_name_plural': 'Продукты', 'verbose_name': 'Продукт'},
        ),
        migrations.AlterField(
            model_name='product',
            name='product_convectors',
            field=models.IntegerField(choices=[(0, '0'), (1, '1'), (2, '2'), (3, '3')], verbose_name='Количество рядов конвекторного оребрения'),
        ),
        migrations.AlterField(
            model_name='product',
            name='product_panels',
            field=models.IntegerField(choices=[(1, '1'), (2, '2'), (3, '3')], verbose_name='Количество панелей'),
        ),
    ]
