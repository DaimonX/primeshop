# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('updated', models.DateTimeField(auto_created=True)),
                ('name', models.CharField(max_length=50, verbose_name='ProductName')),
                ('product_type', models.CharField(max_length=50, verbose_name='ProductType')),
                ('timestamp', models.DateTimeField(auto_now=True)),
            ],
        ),
    ]
