from django.shortcuts import render_to_response, render
from .forms import ProductForm, ContactForm
from django.template import RequestContext
from django.core.mail import send_mail

# Create your views here.


def index(request):
    if request.method == "POST":
        print(request.POST)
    form = ProductForm(request.POST or None)

    if form.is_valid():
        # form.save
        # print request.POST['product_type'] # not reccomend grab this raw data and save it
        instance = form.save(commit=False)
        print(instance)
    title = "Мой тайтл %s" % request.user
    context = {
        "template_title": title,
        "form": form,
    }
    return render_to_response('index.html',
                              context,
                              context_instance=RequestContext(request))


def contact(request):
    form = ContactForm(request.POST or None)
    if form.is_valid():
        print(form.cleaned_data)
    context = {
        "form": form,
        "title":'Свяжитесь с нами'
    }
    # send_mail('Subject here', 'Here is the message.', 'from@example.com',
    # ['to@example.com'], fail_silently=False)
    return render(request, "forms.html", context)
