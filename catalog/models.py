from django.db import models


# Create your models here.


# class Radiator(models.Model):
#     name = models.Charfield(max_length=120, blank=False, null=False)


# def __unicode__(self):
#         return self.name
class Product(models.Model):
    PRODUCT_TYPE_CHOICES = (('ЛУ', 'ЛУ'), ('ЛК', 'ЛК'))
    PRODUCT_PANELS_CHOICES = ((1, '1'), (2, '2'), (3, '3'))
    PRODUCT_CONVECTORS_CHOICES = ((0, '0'), (1, '1'), (2, '2'), (3, '3'))
    name = models.CharField('Имя', max_length=50)
    product_type = models.CharField('Тип', max_length=2, choices=PRODUCT_TYPE_CHOICES)
    product_panels = models.IntegerField('Количество панелей', choices=PRODUCT_PANELS_CHOICES)
    product_convectors = models.IntegerField('Количество рядов конвекторного оребрения', choices=PRODUCT_CONVECTORS_CHOICES)
    timestamp = models.DateTimeField(auto_now=True, auto_created=False)
    updated = models.DateTimeField(auto_now=False, auto_created=True)

    class Meta:
        verbose_name = 'Продукт'
        verbose_name_plural = 'Продукты'
    def __unicode__(self):
        return self.name
