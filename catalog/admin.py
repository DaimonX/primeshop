from django.contrib import admin

# Register your models here

from .forms import ProductForm
from .models import Product


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    fields = ("name", "product_type", "product_panels", "product_convectors", "updated")
    list_display = ("name", "product_type", "product_panels", "product_convectors", "updated")

    form = ProductForm
