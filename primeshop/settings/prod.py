from .base import *
import dj_database_url
import os

# Parse database configuration from $DATABASE_URL

DATABASES['default'] = dj_database_url.config()

# Honor the 'X-Forwarded-Proto' header for request.is_secure()
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

DEBUG = True

ALLOWED_HOSTS = ['*']

SECRET_KEY = 'x^==@zbc75r^ot*0@fep7pshr=60*@i=+p=hd2m@6=54rp0s85'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'dejkrqv6qf3879',
        'USER': 'otffunqlqbvmuw',
        'PASSWORD': 'jAUVZAJEjaLOKgv_qPl8XKKiFX',
        'HOST': 'ec2-54-83-203-50.compute-1.amazonaws.com',
        'PORT': '5432',
    }
}

# Static asset configuration
STATIC_URL = '/static/'
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
STATIC_ROOT = os.path.join(BASE_DIR, "static", "root_static")
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static", "our_static"),
)
