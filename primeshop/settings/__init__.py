from .base import *

# change for dev or prod env:
if os.environ.get('ENVIRONMENT') == 'DEV':
    try:
        from .dev import *
    except ImportError:
        pass
else:
    try:
        from .prod import *
    except ImportError:
        pass

# try:
#     from .prod import *
# except ImportError:
#     pass